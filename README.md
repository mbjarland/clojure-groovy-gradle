# Polyglot clojure-groovy project in gradle
A small project showcasing how you can call groovy code from clojure using gradle. 

Executing: 

    $ ./gradlew

in the root directory will create an executable jar and running: 

    $ $ java -jar build/libs/groovy-clojure-all.jar
    Clojure says Hello to Groovy!

exercises the code. 

# Raison d'être
I wrote this little project to see if it was possible. Seems it is. Not sure if it will be helpful to anybody, but figured I would publish it. Maybe it will save somebody some time. 

# Author
Matias Bjarland
