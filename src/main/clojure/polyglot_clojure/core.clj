(ns polyglot-clojure.core
  (:import (polyglot.groovy GroovyClass))
  (:gen-class))

(defn -main [& args]
  (println "Clojure says" (.sayHello (GroovyClass.))))