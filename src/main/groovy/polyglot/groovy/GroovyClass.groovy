package polyglot.groovy

class GroovyClass { 
  def sayHello() {
    "Hello to Groovy!"
  }
}